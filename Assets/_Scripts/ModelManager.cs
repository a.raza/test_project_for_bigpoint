﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelManager : MonoBehaviour
{
    //Static instance of CubeOperator which allows it to be accessed by any other script.
    public static ModelManager instance = null;

    public MeshFilter m_standerdMesh;

    [Range(0.5f,3)]
    public float m_shapeChangingInterval;
    [Range(100, 300)]
    public float m_rotationSpeed;
    private bool pause = false;

    private Mesh m_mesh;
    private Vector3[] m_verts;
    private int m_currentShape = 0;
    private Vector3 m_defaultRotation;
    private Animator m_anim;

    [SerializeField]
    public List<Shape> m_shapes = new List<Shape>();

    # region UnityCallBacks
    //Awake is always called before any Start functions
    void Awake()
    {
        m_anim = GetComponent<Animator>();

        m_defaultRotation = Vector3.forward;

        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        //Call the InitGame function to initialize the first level 
        StartCoroutine(Init());
    }

    public void Update()
    {
        RotateMain();
    }

    private void RotateMain()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_anim.enabled = true;

            if(UIManager.instance != null)
            {
                UIManager.instance.AddScore();
            }

            pause = true;
        }

        if (pause == false)
        {
            transform.Rotate(m_defaultRotation * m_rotationSpeed * Time.deltaTime);
        }
        else
        {
            m_anim.Play("Display");
        }
    }

    #endregion

    #region ManagingFunctions

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.5f);

        if (m_shapes.Count.Equals(0))
            ErrorLog();

        MakeNewMesh(m_standerdMesh);
    }

    MeshFilter MakeNewMesh(MeshFilter a_mesh)
    {
        //getting the mesh data and adding into a new mesh
        m_mesh = a_mesh.sharedMesh; //sharedmesh is used for reading mesh data

        //creating an array of int,
        //according to the number of triangles in mesh
        var triangles = m_mesh.triangles;

        //creating an array of vecter 3,
        //according to the total number of vertix points in the mesh  
        var vertices = new Vector3[triangles.Length];

        //copying all the vertex points in the mesh, into temp var
        var oldVerts = m_mesh.vertices;

        //Adding all the vertex points into Vector3 array,
        //and triangles in the mesh, into int array, one by one
        for (var i = 0; i < triangles.Length; i++)
        {
            vertices[i] = oldVerts[triangles[i]];

            triangles[i] = i;
        }

        //assigning the vertices of the new mesh with the vector3 array
        m_mesh.vertices = vertices;

        //assigning the triangles of the new mesh with the int array
        m_mesh.triangles = triangles;

        //After modifying the vertices..
        //it is often useful to update the normals to reflect the change
        m_mesh.RecalculateNormals();

        //getting the vertices to update them in realtime..
        m_verts = m_mesh.vertices;

        return a_mesh;
    }

    //Get the vertices of the next shape and convert
    IEnumerator CalcNxtModel()
    {
        yield return new WaitForSeconds(m_shapeChangingInterval);

        var shape = m_shapes[m_currentShape];

        for (var i = 0; i < m_verts.Length; i++)
        {
            if (i == shape.vertices.Length)
            {
                if (m_verts[i] == Vector3.zero)
                    break;

                for (; i < m_verts.Length; i++)
                {
                    m_verts[i] = Vector3.zero;
                }

                break;
            }

            // m_verts[i] = (shape.vertices[i] * 2) // increase the size of the vertices = big 3d model

            m_verts[i] = shape.vertices[i];
        }

        //applying the new positions of vertices
        m_mesh.vertices = m_verts;

        //update the modified mesh
        m_mesh.RecalculateNormals();

        //re-writing the mesh data
        GetComponent<MeshFilter>().sharedMesh = m_mesh;

        m_currentShape++;

        if (m_currentShape >= m_shapes.Count)
            m_currentShape = 0;

        if (pause == false)
            StartCoroutine(CalcNxtModel());
    }

    private void ErrorLog()
    {
        Debug.Log("Please Add Shapes");
        this.enabled = false;
    }

    private float Speed()
    {
        return m_shapeChangingInterval * Time.deltaTime;
    }

    #endregion

    #region AnimationEvent

    public void EndDisplay()
    {
        m_anim.Play("EndDisplay");

        pause = false;
    }

    public void Activate()
    {
        StartCoroutine(CalcNxtModel());

        m_anim.enabled = false;
    }

    #endregion

}

[System.Serializable]
public class Shape
{
    public string shape;
    public Vector3[] vertices;
}
