﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticesReader : MonoBehaviour
{
    private readonly List<Vector3> m_positions = new List<Vector3>();

    private MeshFilter m_mesh;

    #region UnityCallBacks

    void Start()
    {
        AddToManager();
    }

    #endregion

    #region ManagingFunctions

    internal void AddToManager()
    {
        Reader();

        var positions = m_positions.ToArray();

        Shape myShape = new Shape
        {
            shape = this.gameObject.name,
            vertices = positions
        };

        if (ModelManager.instance != null)
            ModelManager.instance.m_shapes.Add(myShape);
    }

    //making new mesh that is writable..
    void Reader()
    {
        m_mesh = GetComponent<MeshFilter>();

        var triangles = m_mesh.mesh.triangles;

        //copying all the vertex points in the mesh, into temp var
        var oldVerts = m_mesh.mesh.vertices;

        //Adding all the vertex points into Vector3 array,
        //and triangles in the mesh, into int array accordingly

        for (var v = 0; v < triangles.Length; v++)
        {
            m_positions.Add(oldVerts[triangles[v]]);

            triangles[v] = v;
        }
    }

    #endregion
}
