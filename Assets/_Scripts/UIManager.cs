﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    //Static instance of CubeOperator which allows it to be accessed by any other script.
    public static UIManager instance = null;

    public Text clickCounter;
    private int count = 0;

    #region UnityCallBacks
    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        //Call the InitGame function to initialize the first level 
        Init();
    }

    #endregion

    #region ManagingFunctions

    private void Init()
    {
        if (clickCounter.Equals(null))
            ErrorLog();
    }

    private void ErrorLog()
    {
        Debug.Log("Please Add UI text");
        this.enabled = false;
    }

    public void AddScore()
    {
        count++;

        clickCounter.text = "Clicked: " + count;
    }

    #endregion
}
